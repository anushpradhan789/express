import express, { json } from "express"
import traineesRouter from "./src/router/traineesRouter.js"
import connectToMongoDb from "./src/connectDb/connectToMongoDb.js"
import studentRouter from "./src/router/studentRouter.js"
import teacherRouter from "./src/router/teacherRouter.js"
import bookRouter from "./src/router/bookRouter.js"
import productRouter from "./src/router/productRouter.js"
import userRouter from "./src/router/userRouter.js"
import reviewRouter from "./src/router/reviewRouter.js"
import bikeRouter from "./src/router/bikeRouter.js"
// make express application

let expressApp = express()
expressApp.use(json())
connectToMongoDb()
// expressApp.use((req,res,next)=>{
//     console.log("I am application middle ware 1")
//     next()
//     },
//     (req,res,next)=>{
//     console.log("I am application middle ware 2")
//     next()
//     }) // this code makes our system to take json data , always place express.json() on top
expressApp.use("/trainees",traineesRouter)
expressApp.use("/students",studentRouter)
expressApp.use("/teachers",teacherRouter)
expressApp.use("/books",bookRouter)
expressApp.use("/products",productRouter)
expressApp.use("/users",userRouter)
expressApp.use("/reviews",reviewRouter)
expressApp.use("/bikes",bikeRouter)


// expressApp.use((req,res,next)=>{
// console.log("I am application middle ware 1")
// },
// (req,res,next)=>{
// console.log("I am application middle ware 2")
// })

// attach port to that
expressApp.listen(8000, ()=>{
    console.log("Express application is listening at port 8000")
})