import bcrypt from "bcrypt"
// $2b$10$GD9uSBFNCc4S2iVwpQqkaukH/CwKUkhT4bl0jPYbRBiu9QITtmksS
// same text has diffrent hash code

/* **** Generate Hash code******** */

/* let password = "Apxd@2980"
let hashedPassword = await bcrypt.hash(password ,10)
console.log(hashedPassword)
 */

/* ****Compare hashpassword and password */
let password = "Apxd@2980"
let validPassword = await bcrypt.compare(password,"$2b$10$GD9uSBFNCc4S2iVwpQqkaukH/CwKUkhT4bl0jPYbRBiu9QITtmksS")
console.log(validPassword)