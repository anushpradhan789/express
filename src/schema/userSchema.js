
/* 

manipulation
trim
lowercase
uppercase
default


validation
required

*********for string
minLength
maxLength
********* for number
min
max


*/
import { Schema } from "mongoose";
import bcrypt from "bcrypt"

let userSchema = Schema({
  name: {
    type: String,
    // lowercase :true,
    // trim :true,
    // required: [true, "this field is required"],
    // minLength :[3,"Minimum length is 3"],
    // maxLength :[20,"max length us 20"],
    // validate :(value)=>{
    //   let isValidName=/^[A-Za-z]+(?: [A-Za-z]+)*$/.test(value)
    //   if (isValidName===true){
    //   }
    //   else {
    //     let error =new Error("Invalid name")
    //     throw error
     

    
  },
  password: {
    type: String,
    // required: [true, "this field is required"],
    // validate:(value)=>{
        
    //   let isValidPassword =/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*])[a-zA-Z\d!@#$%^&*]{8,20}$/.test(value)
    //   if(isValidPassword===true){
    //   }
    //   else
    //   {
    //     let error = new Error ("PASSWORD MUST CONTAIN 1 UPPER CASE, 1 SYMBOL,AND ONE NUMBER")
    //     throw error
    //   }

    // }
  },
  email: {
    type: String,
    // unique: true,
    // required: [true, "this field is required"],
    // validate:(value)=>{
    //   let isValidEmail =/[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}/.test(value)
    //   if(isValidEmail===true){

    //   }
    //   else
    //   {
    //     let error = new Error ("Invalid Email")
    //     throw error
    //   }

    // }
  },
  phoneNumber: {
    type: Number,
    // required: [true, "this field is required"],
    // // min:[9800000000,"Number invalid"],  // easy way
    // // max:[10000000000,"number Invalid"]
    // validate :(value)=>{            //using a function
    //   let Str1= String(value)
    //   let length =Str1.length
    //   if (length!==10){
    //     let error = new Error ("NUmber must be 10 characters long")
    //     throw error
    //   }
    // }
  },
  roll: {
    type: Number,
    min:[1,"Minimum is 1"],
    max :[100,"Maximum is 100"],

  },
  isMarried: {
    type: String,

  },
  spouseName: {
    type: String,

  },

  gender: {
    type: String,
    // required: [true, "this field is required"],
    // validate :(value)=>{
    //   if (value==="male"|| value==="female"|| value === "others"){
    //     this
    //   }
    //     else {let error = new Error ("Gender must be Male, Female or other")
    //     throw error
    //     }

    // }
  },
  dob: {
    type: Date,
    // required: [true, "this field is required"],
  },
  location: {
    country:{
    type: String,
    // required: [true, "this field is required"]},

    exactLocation:{
    type: String,
    // required: [true, "this field is required"]
  },
  },
},

  favTeacher: 
    [
      {
        type:String
      }
    ],
  

  favSubject: [{
    bookName :{
      type : String
    },
  bookAuthor :   {
      type : String
    }
  }
  ]
  
});

export default userSchema;

