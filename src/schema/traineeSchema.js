import { Schema } from "mongoose";

let traineeSchema = Schema({
  name: {
    type: String,
    required: [true, "This field is required"],
    validate :(value)=>{
      let isValidName=/^[A-Za-z]+(?: [A-Za-z]+)*$/.test(value)
      if (isValidName===true){

      }
      else {
        let error =new Error("Invalid name")
        throw error
      }

    }
  },
  class : {
    type: String,
    required: [true, "this field is required"],
  },
  faculty : {
    type: Number,
    required: [true, "this field is required"],
  },
});
export default traineeSchema;

