import { Schema } from "mongoose";

let studentSchema = Schema({
  name: {
    type: String,
    required: [true, "This field is required"],
  },
  age: {
    type: Number,
    required: [true, "this field is required"],
  },
  isMarried: {
    type: Boolean,
    required: [true, "this field is required"],
  },
})
export default studentSchema;

