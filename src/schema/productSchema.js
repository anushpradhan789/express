import { Schema } from "mongoose";

let productSchema = Schema({
  name: {
    type: String,
    required: [true, "This field is required"],
    validate :(value)=>{
      let isValidName=/^[A-Za-z]+(?: [A-Za-z]+)*$/.test(value)
      if (isValidName===true){

      }
      else {
        let error =new Error("Invalid name")
        throw error
      }

    }
  },

  price: {
    type: Number,
    required: [true, "this field is required"],
  },
  quantity: {
    type: Number,
    required: [true, "this field is required"],
  },
});
export default productSchema;

