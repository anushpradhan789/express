import { Schema } from "mongoose";

let bookSchema = Schema({
  name: {
    type: String,
    required: [true, "This field is required"],
  },
  author: {
    type: String,
    required: [true, "this field is required"],
  },
  price: {
    type: Number,
    required: [true, "this field is required"],
  },
  isAvailable: {
    type: Boolean,
    required: [true, "this field is required"],
  },
});
export default bookSchema;

