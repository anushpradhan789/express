import { Schema } from "mongoose";

let reviewSchema = Schema({
  productId: {
    type: Schema.ObjectId,
    ref:"Product",
    required: [true, "This field is required"],
  },

  userId: {
    type: Schema.ObjectId,
    ref :"User",
    required: [true, "this field is required"],
  },
  description: {
    type: String,
    required: [true, "this field is required"],
  },
});
export default reviewSchema;

