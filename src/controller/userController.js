import { User } from "../schema/model.js"
import bcrypt from "bcrypt"
export let userCreateController = async(req,res,next)=>{
    let data =req.body
    data.password = await bcrypt.hash(data.password,10)
    try {

        let result =await User.create(data)

        res.json({
            success :true,
            message :"Student created Successfully",
            result:result
    
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }

}
export let userReadController =async(req,res,next)=>{
    // let result =await User.find({name:"Anush"})
    // let result =await User.find({age:{$gt:25}})
    // let result =await User.find({age:{$gte:25}})
    // let result =await User.find({age:{$lt:25}})
    // let result =await User.find({age:{$lte:25}})
    // let result =await User.find({age:{$ne:25}})
    // let result =await User.find({age:{$gte:15,$lte:17}})
    // let result =await User.find({name:{$in:[Anush,sahil,ram]}})     ********include
    // let result =await User.find({name:{$nin:[Anush,sahil,ram]}}) ********** doesn't include
    // let result =await User.find({name:/anush/})  for Regex search
    // let result =await User.find({name:/anush/i})  for Regex search  [case insensitive]
    // let result =await User.find({name:/^an/i})  for Regex search  [case insensitive starts with an]
    // let result =await User.find({name:/an$/i})  for Regex search  [case insensitive ends with an]


    // let result =await User.find({})  .sort("name") for sorting in ascending order of name
    // let result =await User.find({})  .sort("name age ") for sorting in ascending order of name and of age if there is a conflict
    // let result =await User.find({})  .sort("-name -age ") for sorting in descending  order of name and then age



    // let result =await User.find({}).select("favTeacher name roll")  // valid
    // let result =await User.find({}).select("-favTeacher -name -roll")  // valid
    // let result =await User.find({}).select("favTeacher name -roll")  // invalid

    // do not use - and + in same select except for _id]

    // let result =await User.find({}).skip(2) // *******skips 1st 2 values
    // let result =await User.find({}).limit(2) // *******shows 1st 2 values
    // let result =await User.find({}).limit(2).skip("2") // *******shows 1st 2 values


    /* order of mongoose */
    // FIND ,SORT ,SELECT ,SKIP ,LIMIT
    




    





    try {
       
        res.json({
            success :true,
            message :"Student found Successfully",
            result:result,
        
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }
  

}
export let userSpecificReadController = async(req,res,next)=>{
    try {
        let params =req.params
        let result =await User.findById(params.id)
        res.json({
            success :true,
            message :"Student found Successfully",
            result:result
    
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }

}
export let userUpdateController =async(req,res,next)=>{
    try {
        let params =req.params
        let result =await User.findByIdAndUpdate(params.id,req.body)
        res.json({
            success :true,
            message :"Student found Successfully",
            result: result
    
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }

}
export let userDeleteController =async(req,res,next)=>{
    try {
        let params =req.params
        let result =await User.findByIdAndDelete(params.id,req.body)
        res.json({
            success :true,
            message :"Student Deleted Successfully",
            result:result
    
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }

}

/* 
search => find , select
sort => sort
pagination => limit,skip */