import { Review } from "../schema/model.js"
export let reviewCreateController = async(req,res,next)=>{
    try {
        let data =req.body
        let result =await Review.create(data)
        res.json({
            success :true,
            message :"Review created Successfully",
            result:result
    
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }

}
export let reviewReadController =async(req,res,next)=>{
    try {
        let result =await Review.find({}).populate("productId","name price")
        .populate("userId")
        res.json({
            success :true,
            message :"Review found Successfully",
            result:result,
        
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }
  

}
export let reviewSpecificReadController = async(req,res,next)=>{
    try {
        let params =req.params
        let result =await Review.findById(params.id)
        res.json({
            success :true,
            message :"Review found Successfully",
            result:result
    
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }

}
export let reviewUpdateController =async(req,res,next)=>{
    try {
        let params =req.params
        let result =await Review.findByIdAndUpdate(params.id,req.body)
        res.json({
            success :true,
            message :"Review found Successfully",
            result: result
    
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }

}
export let reviewDeleteController =async(req,res,next)=>{
    try {
        let params =req.params
        let result =await Review.findByIdAndDelete(params.id,req.body)
        res.json({
            success :true,
            message :"Review Deleted Successfully",
            result:result
    
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }

}