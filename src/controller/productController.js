import { Product } from "../schema/model.js"
export let productCreateController = async(req,res,next)=>{
    try {
        let data =req.body
        let result =await Product.create(data)
        res.json({
            success :true,
            message :"Student created Successfully",
            result:result
    
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }

}
export let productReadController =async(req,res,next)=>{
    try {
        let result =await Product.find({})
        res.json({
            success :true,
            message :"Student found Successfully",
            result:result,
        
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }
  

}
export let productSpecificReadController = async(req,res,next)=>{
    try {
        let params =req.params
        let result =await Product.findById(params.id)
        res.json({
            success :true,
            message :"Student found Successfully",
            result:result
    
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }

}
export let productUpdateController =async(req,res,next)=>{
    try {
        let params =req.params
        let result =await Product.findByIdAndUpdate(params.id,req.body)
        res.json({
            success :true,
            message :"Student found Successfully",
            result: result
    
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }

}
export let productDeleteController =async(req,res,next)=>{
    try {
        let params =req.params
        let result =await Product.findByIdAndDelete(params.id,req.body)
        res.json({
            success :true,
            message :"Student Deleted Successfully",
            result:result
    
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }

}