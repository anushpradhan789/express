import { Student } from "../schema/model.js"
export let studentCreateController = async(req,res,next)=>{
    try {
        let data =req.body
        let result =await Student.create(data)
        res.json({
            success :true,
            message :"Student created Successfully",
            result:result
    
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }

}
export let studentReadController =async(req,res,next)=>{
    try {
        let result =await Student.find({})
        res.json({
            success :true,
            message :"Student found Successfully",
            result:result,
        
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }
  

}
export let studentSpecificReadController = async(req,res,next)=>{
    try {
        let params =req.params
        let result =await Student.findById(params.id)
        res.json({
            success :true,
            message :"Student found Successfully",
            result:result
    
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }

}
export let studentUpdateController =async(req,res,next)=>{
    try {
        let params =req.params
        let result =await Student.findByIdAndUpdate(params.id,req.body)
        res.json({
            success :true,
            message :"Student found Successfully",
            result: result
    
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }

}
export let studentDeleteController =async(req,res,next)=>{
    try {
        let params =req.params
        let result =await Student.findByIdAndDelete(params.id,req.body)
        res.json({
            success :true,
            message :"Student Deleted Successfully",
            result:result
    
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }

}