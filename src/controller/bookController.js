import { Book } from "../schema/model.js"
export let bookCreateController = async(req,res,next)=>{
    try {
        let data =req.body
        let result =await Book.create(data)
        res.json({
            success :true,
            message :"Student created Successfully",
            result:result
    
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }

}
export let bookReadController =async(req,res,next)=>{
    try {
        let result =await Book.find({})
        res.json({
            success :true,
            message :"Student found Successfully",
            result:result,
        
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }
  

}
export let bookSpecificReadController = async(req,res,next)=>{
    try {
        let params =req.params
        let result =await Book.findById(params.id)
        res.json({
            success :true,
            message :"Student found Successfully",
            result:result
    
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }

}
export let bookUpdateController =async(req,res,next)=>{
    try {
        let params =req.params
        let result =await Book.findByIdAndUpdate(params.id,req.body)
        res.json({
            success :true,
            message :"Student found Successfully",
            result: result
    
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }

}
export let bookDeleteController =async(req,res,next)=>{
    try {
        let params =req.params
        let result =await Book.findByIdAndDelete(params.id,req.body)
        res.json({
            success :true,
            message :"Student Deleted Successfully",
            result:result
    
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }

}