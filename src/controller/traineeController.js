import { Trainee } from "../schema/model.js"
export let traineeCreateController = async(req,res,next)=>{
    try {
        let data =req.body
        let result =await Trainee.create(data)
        res.json({
            success :true,
            message :"Student created Successfully",
            result:result
    
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }

}
export let traineeReadController =async(req,res,next)=>{
    try {
        let result =await Trainee.find({})
        res.json({
            success :true,
            message :"Student found Successfully",
            result:result,
        
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }
  

}
export let traineeSpecificReadController = async(req,res,next)=>{
    try {
        let params =req.params
        let result =await Trainee.findById(params.id)
        res.json({
            success :true,
            message :"Student found Successfully",
            result:result
    
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }

}
export let traineeUpdateController =async(req,res,next)=>{
    try {
        let params =req.params
        let result =await Trainee.findByIdAndUpdate(params.id,req.body)
        res.json({
            success :true,
            message :"Student found Successfully",
            result: result
    
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }

}
export let traineeDeleteController =async(req,res,next)=>{
    try {
        let params =req.params
        let result =await Trainee.findByIdAndDelete(params.id,req.body)
        res.json({
            success :true,
            message :"Student Deleted Successfully",
            result:result
    
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }

}