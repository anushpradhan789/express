import { Teacher } from "../schema/model.js"
export let teacherCreateController = async(req,res,next)=>{
    try {
        let data =req.body
        let result =await Teacher.create(data)
        res.json({
            success :true,
            message :"Teacher created Successfully",
            result:result
    
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }

}
export let teacherReadController =async(req,res,next)=>{
    try {
        let result =await Teacher.find({})
        res.json({
            success :true,
            message :"Teacher found Successfully",
            result:result,
        
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }
  

}
export let teacherSpecificReadController = async(req,res,next)=>{
    try {
        let params =req.params
        let result =await Teacher.findById(params.id)
        res.json({
            success :true,
            message :"Teacher found Successfully",
            result:result
    
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }

}
export let teacherUpdateController =async(req,res,next)=>{
    try {
        let params =req.params
        let result =await Teacher.findByIdAndUpdate(params.id,req.body)
        res.json({
            success :true,
            message :"Teacher found Successfully",
            result: result
    
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }

}
export let teacherDeleteController =async(req,res,next)=>{
    try {
        let params =req.params
        let result =await Teacher.findByIdAndDelete(params.id,req.body)
        res.json({
            success :true,
            message :"Teacher Deleted Successfully",
            result:result
    
        })
    } catch (error) {
        res.json({
            success :false,
            message :error.message
        })
        
    }

}