import { Router } from "express";
import { userCreateController, userDeleteController, userReadController, userSpecificReadController, userUpdateController } from "../controller/userController.js";


let userRouter = Router()
userRouter.route("/")
.post(userCreateController)
.get(userReadController)
userRouter.route("/:id")
.get(userSpecificReadController)
.patch(userUpdateController)
.delete(userDeleteController)
export default userRouter