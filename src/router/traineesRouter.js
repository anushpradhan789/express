import { Router } from "express";
import { traineeCreateController, traineeDeleteController, traineeReadController, traineeSpecificReadController, traineeUpdateController } from "../controller/traineeController.js";

let traineeRouter = Router()
traineeRouter.route("/")
.post(traineeCreateController)
.get(traineeReadController)
traineeRouter.route("/:id")
.get(traineeSpecificReadController)
.patch(traineeUpdateController)
.delete(traineeDeleteController)
export default traineeRouter