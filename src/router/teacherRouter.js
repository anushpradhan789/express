import { Router } from "express"
import { Teacher } from "../schema/model.js"

let teacherRouter = Router()
teacherRouter.route("/")
.post((req,res,next)=>{
    let data =req.body
    let result = Teacher.create(data)

    res.json({
        success :true,
        message :"Teacher created Successfully"

    })
})
export default teacherRouter