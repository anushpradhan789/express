import { Router } from "express";
import { productCreateController, productDeleteController, productReadController, productSpecificReadController, productUpdateController } from "../controller/productController.js";


let productRouter = Router()
productRouter.route("/")
.post(productCreateController)
.get(productReadController)
productRouter.route("/:id")
.get(productSpecificReadController)
.patch(productUpdateController)
.delete(productDeleteController)
export default productRouter