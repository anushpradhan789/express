import { Router } from "express";
import { reviewCreateController, reviewDeleteController, reviewReadController, reviewSpecificReadController, reviewUpdateController } from "../controller/reviewController.js";


let reviewRouter = Router()
reviewRouter.route("/")
.post(reviewCreateController)
.get(reviewReadController)
reviewRouter.route("/:id")
.get(reviewSpecificReadController)
.patch(reviewUpdateController)
.delete(reviewDeleteController)
export default reviewRouter