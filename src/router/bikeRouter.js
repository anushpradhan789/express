import { Router } from "express";

let bikeRouter =Router()
// let createBike =(req,res,next)=>{
//     console.log("Bike created successfully")
// c
// }
let createBike=(a,b)=>{
  return  (req,res,next)=>{
    console.log("bikes create successfully")

    }
}

bikeRouter
.route("/")
.post(createBike(1,2)) //use this format if you need to pass value
// .post(createBike)
.get((req,res,next)=>{
    res.json("get bikes")
})
.patch((req,res,next)=>{
    res.json("patch bikes")
})
.delete((req,res,next)=>{
    res.json("delete bikes")
})

bikeRouter
.route("/:id")
.post((req,res,next)=>{
    console.log(req.params) // req.param it gives dynamic route parameter in the form of object
    res.json("post bikes")
})
.get((req,res,next)=>{
    res.json("get bikes")
})
.patch((req,res,next)=>{
    res.json("patch bikes")
})
.delete((req,res,next)=>{
    res.json("delete bikes")
})


bikeRouter
.route("/a/:id1/name/:id2") // here a and name is static and :id1 and Lid2 is dynamic route param
.post((err,req,res,next)=>{
    res.json("post bikes")  
    
    console.log(req.query) // for query ?name=ap&age=19
    console.log(req.params)
    console.log(req.body)
})
.get((req,res,next)=>{
    res.json("get bikes")
})
.patch((req,res,next)=>{
    res.json("patch bikes ")
})
.delete((req,res,next)=>{
    res.json("a")
    let err1 = new Error("I am Error")
    console.log("i am middleware 1")
    next(err1)
},(err,req,res,next)=>{
    console.log("i am middleware 2")
    console.log(err.message)
    next()                               //next is used to trigger next middleware(=> function)
},
(req,res,next)=>{
    console.log("i am middleware 3 ")
})


 export default bikeRouter