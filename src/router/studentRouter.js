import { Router } from "express";
import { studentCreateController, studentDeleteController, studentReadController, studentSpecificReadController, studentUpdateController } from "../controller/studentController.js";

let studentRouter = Router()
studentRouter.route("/")
.post(studentCreateController)
.get(studentReadController)
studentRouter.route("/:id")
.get(studentSpecificReadController)
.patch(studentUpdateController)
.delete(studentDeleteController)
export default studentRouter