import { Router } from "express";
import { bookCreateController, bookDeleteController, bookReadController, bookSpecificReadController, bookUpdateController } from "../controller/bookController.js";


let bookRouter = Router()
bookRouter.route("/")
.post(bookCreateController)
.get(bookReadController)
bookRouter.route("/:id")
.get(bookSpecificReadController)
.patch(bookUpdateController)
.delete(bookDeleteController)
export default bookRouter